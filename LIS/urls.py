"""LIS URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import patterns as patterns

from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url, include
from django.conf.urls import handler404
from django.urls import path, re_path
from django.contrib.staticfiles import views
from rest_framework_jwt.views import obtain_jwt_token

from LIS.apps.accounts.views import *

from LIS.apps.sora.orderComments import *

app_name = "LIS"

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(('LIS.apps.blog.posts.urls', 'posts'), namespace='posts')),
    path('order/app/', include(("LIS.apps.sora.app.urls", 'order'), namespace='order')),
    path('readers/', include(("LIS.apps.Lib.urls", 'lib'), namespace='lib')),
    path('comments/', include(('LIS.apps.blog.comments.urls', 'comments'), namespace="comments")),
    path('register/', register_view, name='register'),
    path('login/', login_view, name='login'),
    path('logout/', logout_view, name='logout'),
    path('api/auth/token/', obtain_jwt_token),
    path('api/users/', include(("LIS.apps.accounts.api.urls", 'users-api'), namespace='users-api')),
    path('api/comments/', include(("LIS.apps.blog.comments.api.urls", 'comments-api'), namespace='comments-api')),
    path('api/posts/', include(("LIS.apps.blog.posts.api.urls", 'posts-api'), namespace='posts-api')),
    # url(r'^posts/$', "<appname>.views.<function_name>"),
]


if settings.DEBUG:
    urlpatterns += [
        re_path(r'^static/(?P<path>.*)$', views.serve),
    ]
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
