# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases
from .base import *
from .assembly import *
'''
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': ('/home/artem/Dev/DB/db.sqlite3'),
    }
}
'''
"""

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}
"""

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'LIS',
        'USER': 'postgres',
        'PASSWORD': 'postgresdb',
        'HOST': '172.17.0.6',
        'PORT': '5432',
    }
}
