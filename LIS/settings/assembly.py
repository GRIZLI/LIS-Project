import os

from .base import *
from .databases import*
from .timezone import *

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '#ncqep0b*rb*3%7yvdf#4@^sn!xpb65qmdo6pskr&ph&ikcpb&'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

if DEBUG == True:
    ALLOWED_HOSTS = ['localhost', '127.0.0.1', '192.168.10.110', '10.1.42.110', '172.17.0.1']
else:
    ALLOWED_HOSTS = ['192.168.10.110', '10.1.42.110']
