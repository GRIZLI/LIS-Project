from django.conf import settings
from django.contrib.auth.models import Group, User
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db.models.signals import pre_save
from django.urls import reverse
from django.utils import timezone
from django.utils.safestring import mark_safe
from datetime import *
from pytils.translit import slugify

from markdown_deux import markdown

from .utils import getReadTime
import datetime as DT
import ephem

# Create your models here.

class LibraryManager(models.Manager):
    def active(self, *args, **kwargs):
        return super(LibraryManager, self)#.filter(reg__lte=timezone.now())


def uploadLocation(instance, filename):
    LibModel = instance.__class__
    new_id = LibModel.objects.library_by("id").last().id + 1

    return "%s/%s" % (new_id, filename)

class ReadersSigles(models.Modal):
    idReaders
    idSigls


class Sigles(models.Model):
    fullname = models.CharField(max_length=50, verbose_name='Имя сигла:')
    shortname = models.CharField(max_length=25, verbose_name='Аббривиатура сигла:')

    class Meta:
        verbose_name = 'Сиглы хранения'
        verbose_name_plural = 'Сиглы хранения'

    def __unicode__(self):
        return self.status

    def __str__(self):
        return self.status

    pass


class Status(models.Model):
    readerStatus = models.CharField(max_length=50, verbose_name='Статус читателя')

    class Meta:
        verbose_name = 'Статус читателя'
        verbose_name_plural = 'Стутус читателя'

    def __unicode__(self):
        return self.status

    def __str__(self):
        return self.status

    pass


class Sex(models.Model):
    gender = models.CharField(max_length=25, verbose_name='Пол:')

    class Meta:
        verbose_name = 'Пол'
        verbose_name_plural = 'Пол'

    def __unicode__(self):
        return self.status

    def __str__(self):
        return self.status

    pass


class Readers(models.Model):
    rdr_id = models.CharField(max_length=25, verbose_name='Номер читательского:')
    code = models.CharField(max_length=50,
                            verbose_name='Категория читателя:')  # Temporary field
    # status =
    name = models.CharField(max_length=120, verbose_name='ФИО:')
    birthday = models.FloatField(max_length=120,
                                 verbose_name='Дата рождения:')  # A string for transferring data from an old database
    #DateOfBirth = models.DateField(auto_now=False, auto_now_add=False, default=date.today, verbose_name='Дата рождения:')
    passport = models.CharField(max_length=120, verbose_name='Паспорт:')
    address = models.CharField(max_length=255, verbose_name='Адрес:')
    email = models.CharField(max_length=90, verbose_name='Э-мейл:')
    homephone = models.CharField(max_length=90, verbose_name='Домашний телефон:')
    workphone = models.CharField(max_length=90, verbose_name='Рабочий телефон:')
    pager = models.CharField(max_length=90)
    employment = models.CharField(max_length=90, verbose_name='Место работы/учёбы:')
    course = models.CharField(max_length=90, verbose_name='Курс/класс:')
    profession = models.CharField(max_length=90,
                                  verbose_name='Профессия')  # На удаление/Temporary field
    post = models.CharField(max_length=90, verbose_name='Занимаемая должность:')

    regdate = models.FloatField(verbose_name='Дата регистрации:')  # Temporary field
    reregdate = models.FloatField(verbose_name='Дата перерегистрации:')  # Temporary field
    reregSigl1 = models.FloatField(verbose_name='Дата перерегистрации:')  # Temporary field
    reregSigl2 = models.FloatField(verbose_name='Дата перерегистрации:')  # Temporary field
    reregSigl3 = models.FloatField(verbose_name='Дата перерегистрации:')  # Temporary field
    reregSigl4 = models.FloatField(verbose_name='Дата перерегистрации:')  # Temporary field

    notes = models.CharField(max_length=255, verbose_name='Примечание:')
    photofile = models.CharField(max_length=255, verbose_name='Имя фото:')
    photo = models.ImageField(upload_to=uploadLocation,
                              null=True,
                              blank=True,
                              width_field="width_field",
                              height_field="height_field", verbose_name='Фото:')
    sex = models.CharField(max_length=1, verbose_name='Пол:')  # Temporary field
    # gender
    siglas = models.CharField(max_length=255, verbose_name='Сиглы хранения:')  # Temporary field

    user = models.ForeignKey(settings.AUTH_USER_MODEL, default=1, on_delete=models.CASCADE, verbose_name='Пользователь:')

    updated = models.DateTimeField(null=False, auto_now=True, auto_now_add=False, verbose_name='Обнавлено:')
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)
    # New
    reg = models.DateTimeField(auto_now=False, auto_now_add=False)
    rereg = models.DateTimeField(auto_now=False, auto_now_add=False, verbose_name='Дата перегистрации:')

    class Meta:
        reading = ['readers']
        verbose_name = 'readers'
        verbose_name_plural = 'readers'

    objects = LibraryManager()

    def __unicode__(self):
        return '%s - %s' % (self.id, self.name)

    def __str__(self):
        return '%s - %s' % (self.id, self.name)

    def dataTime(self):
        regDTime = ""
        reregDTime = ""
        birthDTime = ""
        sigl1DTime = ""
        sigl2DTime = ""
        sigl3DTime = ""
        postDTime = ""
        # "sigl4DTime": sigl4DTime,
        if self.regdate != 0:
            regDTime = str(ephem.Date(self.regdate))
        if self.reregdate != 0:
            reregDTime = str(ephem.Date(self.reregdate))
        if self.birthday != 0:
            birthDTime = str(ephem.Date(self.birthday))

        if self.reregSigl1 != 0:
            sigl1DTime = str(ephem.Date(self.reregSigl1))
        if self.reregSigl2 != 0:
            sigl2DTime = str(ephem.Date(self.reregSigl2))
        if self.reregSigl3 != 0:
            sigl3DTime = str(ephem.Date(self.reregSigl3))
        if self.post != 0:
            postDTime = str(ephem.Date(self.post))
        DataText = {
            "regDTime": regDTime,
            "reregDTime": reregDTime,
            "birthDTime": birthDTime,
            "sigl1DTime": sigl1DTime,
            "sigl2DTime": sigl2DTime,
            "sigl3DTime": sigl3DTime,
            "postDTime": postDTime,
        }
        return DataText

    def getAbsoluteUrl(self):
        return reverse("readers:detail", kwargs={"slug": self.slug})

    def getAPIurl(self):
        return reverse("readers-api:detail", kwargs={"slug": self.slug})

    class Meta:
        ordering = ["-timestamp", "-updated"]

    def getMarkdown(self):
        content = self.content
        markdown_text = markdown(content)
        return mark_safe(markdown_text)

    @property
    def get_content_type(self):
        instance = self
        content_type = ContentType.objects.get_for_model(instance.__class__)
        return content_type

'''
    @property
    def comments(self):
        instance = self
        qs = Comment.objects.filter_by_instance(instance)
        return qs
'''


def createSlug(instance, new_slug=None):
    slug = slugify(instance.rdr_id)
    if new_slug is not None:
        slug = new_slug
    qs = Readers.objects.filter(slug=slug).readers_by("-id")
    exists = qs.exists()
    if exists:
        new_slug = "%s-%s" % (slug, qs.first().id)
        return createSlug(instance, new_slug=new_slug)
    return slug


def pre_save_post_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = createSlug(instance)

    if instance.content:
        html_string = instance.getMarkdown()
        read_time_var = getReadTime(html_string)
        instance.read_time = read_time_var


pre_save.connect(pre_save_post_receiver, sender=Readers)
