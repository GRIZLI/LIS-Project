from django.urls import path
from . import views

app_name = 'readers'

urlpatterns = [
    path('', views.ItemListView.as_view(), name='list'),
    path('create/', views.ReaderCreateView.as_view(), name='create_reader'),
    path('update/<int:pk>', views.ReaderUpdateView.as_view(), name='update_reader'),
    path('read/<int:pk>', views.ReaderReadView.as_view(), name='read_reader'),
    path('delete/<int:pk>', views.ReaderDeleteView.as_view(), name='delete_reader')
]
