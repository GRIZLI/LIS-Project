from django import forms

from pagedown.widgets import PagedownWidget
from .models import Readers
from django import forms
from bootstrap_modal_forms.mixins import PopRequestMixin, CreateUpdateAjaxMixin


class ReadersForms(PopRequestMixin, CreateUpdateAjaxMixin, forms.ModelForm):
    rdr_id = forms.CharField(label='Читательский:')
    name = forms.CharField(label='ФИО:')

    class Meta:
        model = Readers
        fields = [
            "rdr_id",
            "name",
            "passport",
            "address",
            "email",
            "homephone",
            "workphone",
            "employment",
            "course",
            "profession",
        ]
