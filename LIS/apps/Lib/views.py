# Create your views here.
from django.urls import reverse_lazy

from LIS.apps.Lib.forms import ReadersForms
from LIS.apps.Lib.models import Readers

from django.db.models import Q
from django.utils import timezone
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render
from django.http import Http404, JsonResponse
from django.contrib.messages.views import SuccessMessageMixin
from django.views import generic
from django.views.generic import ListView
from django.template.loader import render_to_string
from bootstrap_modal_forms.mixins import PassRequestMixin, DeleteAjaxMixin


class ItemListView(ListView):
    template_name = 'lib/readers_list.html'

    def get(self, request):
        if request.user.is_staff or request.user.is_superuser:
            queryset_list = Readers.objects.all()
        else:
            raise Http404

        query = request.GET.get("q")
        if query:
            queryset_list = queryset_list.filter(
                Q(rdr_id__icontains=query) |
                Q(name__icontains=query)
            ).distinct()

        return render(
            request=request,
            template_name=self.template_name,
            context=
            {
                "title": "[LIS]: Читатели",
                'object_list': get_paginated_page(request, queryset_list),
            })

    def post(self, request):
        if request.is_ajax():
            return JsonResponse({
                "result": True,
                "articles": render_to_string(
                    request=request,
                    template_name='list/readers_list.html',
                    context={'object_list': get_paginated_page(request, Readers.objects.all())}
                )
            })
        else:
            raise Http404()


def get_paginated_page(request, objects, number=10):
    current_page = Paginator(objects, number)
    page = request.GET.get('page') if request.method == 'GET' else request.POST.get('page')
    try:
        return current_page.page(page)
    except PageNotAnInteger:
        return current_page.page(1)
    except EmptyPage:
        return current_page.page(current_page.num_pages)


# Create
class ReaderCreateView(PassRequestMixin, SuccessMessageMixin, generic.CreateView):
    template_name = 'lib/modals/create_reader.html'
    form_class = ReadersForms
    success_message = 'Читатель успешно создан.'
    success_url = reverse_lazy('lib:list')


# Update
class ReaderUpdateView(PassRequestMixin, SuccessMessageMixin, generic.UpdateView):
    model = Readers
    template_name = 'lib/modals/update_reader.html'
    form_class = ReadersForms
    success_message = 'Данные читателя были успешно отредактированы и сохранены.'
    success_url = reverse_lazy('lib:list')


# Read
class ReaderReadView(generic.DetailView):
    model = Readers
    form_class = ReadersForms
    template_name = 'lib/modals/read_reader.html'


# Delete
class ReaderDeleteView(DeleteAjaxMixin, generic.DeleteView):
    model = Readers
    template_name = 'lib/modals/delete_reader.html'
    success_message = 'Читатель был удалён.'
    success_url = reverse_lazy('lib:list')


class ReadersListView(ListView):
    model = Readers
    template_name = 'lib/readers_list.html'


class ViewReadersForm:
    today = timezone.now().date()
    model = Readers
