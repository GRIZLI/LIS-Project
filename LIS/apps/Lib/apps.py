from django.apps import AppConfig


class LibConfig(AppConfig):
    name = 'LIS.apps.Lib'
