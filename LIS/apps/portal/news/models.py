from django.db import models


class News(models.Models):
    class Meta():
        db_table = "PortalNews"  # Название базы данных

    newsTitle = models.CharField(max_length=200)
    newsText = models.TextField()
    newsDate = models.TimeField()
    newsLikes = models.Integer()

