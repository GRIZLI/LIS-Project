from django.conf.urls import url
from django.contrib import admin
from django.urls import path

from .views import (
    PostCreateAPIView,
    PostDeleteAPIView,
    PostDetailAPIView,
    PostListAPIView,
    PostUpdateAPIView,
    )

urlpatterns = [
    path('', PostListAPIView.as_view(), name='list'),
    path('create/', PostCreateAPIView.as_view(), name='create'),
    path('<str:slug>/', PostDetailAPIView.as_view(), name='detail'),
    path('<str:slug>/edit/', PostUpdateAPIView.as_view(), name='update'),
    path('<str:slug>/delete/', PostDeleteAPIView.as_view(), name='delete'),
]