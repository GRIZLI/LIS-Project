from urllib.parse import quote_plus #python3

from django import template


register = template.Library()

@register.filter
def urlify(value):
    return quote_plus(value)
