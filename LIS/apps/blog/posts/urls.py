from django.conf.urls import url

from django.contrib import admin
from django.urls import path

from . import views

urlpatterns = [
    path('', views.post_list, name='list'),
    path('create/', views.post_create, name='create'),
    path('<str:slug>', views.post_detail, name='detail'),
    path('<str:slug>/edit/', views.post_update, name='update'),
    path('<str:slug>/delete/', views.post_delete),
    # url(r'^posts/$', "<appname>.views.<function_name>"),
]
