import logging

from django.contrib.auth import (authenticate, get_user_model, login, logout)
from django.shortcuts import render, redirect
from .forms import UserLoginForm, UserRegisterForm

log = logging.getLogger(__name__)


def login_view(request):
    log.info('[acc.views.login_view]: Выполняется попытка авторизации...')
    print(request.user.is_authenticated)
    next = request.GET.get('next')
    title = "Авторизация пользователя"
    button = "Войти"
    log.info('[acc.views.login_view]: Выполняется функция GET...')
    form = UserLoginForm(request.POST or None)
    if form.is_valid():
        log.info('[acc.views.login_view]: Выполняется сравнение введенных данных...')
        username = form.cleaned_data.get("username")
        password = form.cleaned_data.get('password')
        user = authenticate(username=username, password=password)
        login(request, user)
        if next:
            log.info('[acc.views.login_view]: ' + username + ' успешно авторизовался')
            return redirect(next)
        return redirect("/")
    context = {
        "form": form,
        "title": title,
        "button": button
    }
    return render(request, "accounts/form.html", context)


def register_view(request):
    log.info('[acc.views.register_view]: Выполняется попытка регистрации...')
    print(request.user.is_authenticated)
    next = request.GET.get('next')
    title = "Регистрация пользователя"
    button = "Принять"
    log.info('[acc.views.register_view]: Выполняется функция GET...')
    form = UserRegisterForm(request.POST or None)
    log.info('[acc.views.register_view]: Выполняется функция POST...')
    if form.is_valid():
        log.info('[acc.views.register_view]: Выполняется проверка на корректность ввода...')
        user = form.save(commit=False)
        password = form.cleaned_data.get('password')
        user.set_password(password)
        user.save()
        new_user = authenticate(username=user.username, password=password)
        login(request, new_user)
        if next:
            return redirect(next)
        return redirect("/")

    context = {
        "form": form,
        "title": title,
        "button": button
    }
    return render(request, "accounts/form.html", context)


def logout_view(request):
    log.info('[acc.views.logout_view]: Выполняется выход из под учётной записи...')
    logout(request)
    log.info('[acc.views.logout_view]: Выход произведён успешно.')
    return redirect("/")


''''
# Регистрация пользователя
def register(request):
    form = UserCreationForm()

    if request.method == 'POST':
        data = request.POST.copy()
        errors = form.get_validation_errors(data)
        if not errors:
            new_user = form.save(data)
            return HttpResponseRedirect("/books/")
    else:
        data, errors = {}, {}

    return render_to_response("accounts/register.html", {
        'form' : forms.FormWrapper(form, data, errors)
    })

# Авторизация пользователя
def login(request):
    username = request.POST['username']
    password = request.POST['password']
    user = auth.authenticate(username=username, password=password)
    if user is not None and user.is_active:
        # Правильный пароль и пользователь "активен"
        auth.login(request, user)
        # Перенаправление на "правильную" страницу
        return HttpResponseRedirect("/accounts/loggedin/")
    else:
        # Отображение страницы с ошибкой
        return HttpResponseRedirect("/accounts/invalid/")

# Выйти из под авторизованного пользователя
def logout(request):
    auth.logout(request)
    # Перенаправление на страницу.
    return HttpResponseRedirect("/accounts/loggedout/")
'''''
