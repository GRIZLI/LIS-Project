from django.conf import settings
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required


@login_required
def PersonalAreaViews(request):
    if not request.user.is_authenticated():
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))


