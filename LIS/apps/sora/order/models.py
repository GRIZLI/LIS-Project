
from __future__ import unicode_literals
from django.db import models
from django.db.models.signals import pre_save
from django.utils.safestring import mark_safe
#from django.utils.text import slugify
from django.utils import timezone

from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.urls import reverse
from django.db import models

from pytils.translit import slugify
from markdown_deux import markdown
from LIS.apps.sora.orderComments.models import *
from .utils import get_read_time


class OrderManager(models.Manager):
    def active(self, *args, **kwargs):
        # Post.objects.all() = super(PostManager, self).all()
        return super(OrderManager, self).filter(draft=False).filter(publish__lte=timezone.now())


def upload_location(instance, filename):
    OrderModel = instance.__class__
    new_id = OrderModel.objects.order_by("id").last().id + 1
    return "%s/%s" % (new_id, filename)


class Department(models.Model):
    name = models.CharField(max_length=50)
    branch = models.TextField(max_length=100)


class Order(models.Model):
    title = models.CharField(max_length=100)  # Заголовок заявки
    Order = models.TextField()  # Текст заявки

    updated = models.DateTimeField(auto_now=True, auto_now_add=False)

    publish = models.DateField(auto_now=True, auto_now_add=True)  # Дата публикации заявки
    readTime = models.IntegerField(default=0)  # Колличество просмотров заявки
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)  # Дата и время последнего редактирования

    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)  # Время создания заявки

    objects = OrderManager()

    def __unicode__(self):
        return self.title

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("order:detail", kwargs={"slug": self.slug})

    def get_api_url(self):
        return reverse("order-api:detail", kwargs={"slug": self.slug})

    class Meta:
        ordering = ["-timestamp", "-updated"]

    def get_markdown(self):
        content = self.content
        markdown_text = markdown(content)
        return mark_safe(markdown_text)

    @property
    def comments(self):
        instance = self
        qs = OrderComment.objects.filter_by_instance(instance)
        return qs

    @property
    def get_content_type(self):
        instance = self
        content_type = ContentType.objects.get_for_model(instance.__class__)
        return content_type


def create_slug(instance, new_slug=None):
    slug = slugify(instance.title)
    if new_slug is not None:
        slug = new_slug
    qs = Order.objects.filter(slug=slug).order_by("-id")
    exists = qs.exists()
    if exists:
        new_slug = "%s-%s" % (slug, qs.first().id)
        return create_slug(instance, new_slug=new_slug)
    return slug


def pre_save_post_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = create_slug(instance)

    if instance.content:
        html_string = instance.get_markdown()
        read_time_var = get_read_time(html_string)
        instance.read_time = read_time_var


pre_save.connect(pre_save_post_receiver, sender=Order)
