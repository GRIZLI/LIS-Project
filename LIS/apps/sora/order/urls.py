from django.conf.urls import url

from django.contrib import admin
from django.urls import path

from . import views

urlpatterns = [
    path('received/', views.viewsResOrder),
    path('', views.orderList, name='list'),

    path('create/', views.orderCreate),
    path('<str:slug>', views.order_detail, name='detail'),
    path('<str:slug>/edit/', views.order_update, name='update'),
    path('<str:slug>/delete/', views.order_delete),
]
