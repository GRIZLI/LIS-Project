from django import forms

from pagedown.widgets import PagedownWidget

from .models import Order


class OrderForm(forms.ModelForm):
    statusFild = (
        'в обработке',
        'принята',
        'отказано',
        'выполнено',
    )
    dep = (     # Отделы
        "",
        ""
    )

    order = forms.CharField(widget=PagedownWidget(show_preview=False))  # Текст заявки
    publish = forms.DateField(widget=forms.SelectDateWidget)  # Дата публикации
    status = forms.CharField(max_length=13, choices=statusFild)  # Статус выполнения заявки
    sender = forms.CharField(max_length=50)  # Отправитель заявки
    senderDep = forms.CharField(max_length=50)  # Отдел отправитель заявки
    depReceiver = forms.CharField(max_length=50, choices=dep)  # Отделы

    class Meta:
        model = Order
        fields = [
            "title",
            "content",
            "draft",
            "publish",
        ]
