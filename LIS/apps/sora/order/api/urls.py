from django.conf.urls import url
from django.contrib import admin

from .views import (
    OrderCreateAPIView,
    OrderDeleteAPIView,
    OrderDetailAPIView,
    OrderListAPIView,
    OrderUpdateAPIView,
    )

urlpatterns = [
    url(r'^$', OrderListAPIView.as_view(), name='list'),
    url(r'^create/$', OrderCreateAPIView.as_view(), name='create'),
    url(r'^(?P<slug>[\w-]+)/$', OrderDetailAPIView.as_view(), name='detail'),
    url(r'^(?P<slug>[\w-]+)/edit/$', OrderUpdateAPIView.as_view(), name='update'),
    url(r'^(?P<slug>[\w-]+)/delete/$', OrderDeleteAPIView.as_view(), name='delete'),
]