from django.conf.urls import url
from django.contrib import admin

from .views import commentThread, commentDelete


urlpatterns = [
    url(r'^(?P<id>\d+)/$', commentThread, name='thread'),
    url(r'^(?P<id>\d+)/delete/$', commentDelete, name='delete'),
]
