from django import forms


class OrderCommentForm(forms.Form):
    contentType = forms.CharField(widget=forms.HiddenInput)
    objectId = forms.IntegerField(widget=forms.HiddenInput)
    content = forms.CharField(label='', widget=forms.Textarea)

