from django.contrib import admin

# Register your models here.
from .models import OrderComment

admin.site.register(OrderComment)
