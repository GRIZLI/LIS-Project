from django.apps import AppConfig


class OrderCommentsConfig(AppConfig):
    name = 'orderComments'
