from django import forms

from pagedown.widgets import PagedownWidget
from .models import Order


class OrderForms(forms.ModelForm):

    content = forms.CharField(widget=PagedownWidget(show_preview=False))
    publish = forms.DateField(widget=forms.SelectDateWidget)

    class Meta:
        model = Order
        fields = [
            "title",  # Заголовок заявки
            "content",  # Текст сообщения
            "image",    # Изображение
            "depRec",   # Отдел получатель
            #"depSender", # Отдел отправитель
            "publish"  # Дата публикации
        ]


