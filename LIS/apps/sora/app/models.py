from django.conf import settings
from django.contrib.auth.models import Group, User
from django.contrib.contenttypes.models import ContentType
from django.urls import reverse
from django.db import models
from pytils.translit import slugify
from datetime import *
from django.db.models.signals import pre_save
from django.utils import timezone
from django.utils.safestring import mark_safe
#from django.utils.text import slugify

from markdown_deux import markdown

from .utils import getReadTime
from LIS.apps.blog.comments.models import *  # Временная заплатка


class OrderManager(models.Manager):
    def active(self, *args, **kwargs):
        # Post.objects.all() = super(PostManager, self).all()
        return super(OrderManager, self).filter(publish__lte=timezone.now())
        # return super(OrderManager, self).filter(draft=False).filter(publish__lte=timezone.now())


def uploadLocation(instance, filename):
    OrderModel = instance.__class__
    new_id = OrderModel.objects.order_by("id").last().id + 1

    return "%s/%s" % (new_id, filename)


class Statuses(models.Model):
    status = models.CharField(max_length=50, verbose_name='Статус сообщения')
   # orderComments = models.CharField(default=0, max_length=255, verbose_name='Коментарий к заявки:')
    class Meta:
        verbose_name = 'Статус сообщения'
        verbose_name_plural = 'Статус сообщения'

    def __unicode__(self):
        return self.status

    def __str__(self):
        return self.status
    pass


class Order(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, default=1, on_delete=models.CASCADE, verbose_name='Пользователь:')

    title = models.CharField(max_length=120, verbose_name='Заголовок:')
    slug = models.SlugField(unique=True, verbose_name='Ссылка:')

    image = models.ImageField(upload_to=uploadLocation,
                              null=True,
                              blank=True,
                              width_field="width_field",
                              height_field="height_field", verbose_name='Изображени:')
    height_field = models.IntegerField(default=0, verbose_name='Высота поля:')
    width_field = models.IntegerField(default=0, verbose_name='Ширина поля:')

    content = models.TextField(verbose_name='Заявка:')

    #depSender = models.CharField(max_length=60, verbose_name='Отдел отправитель')
    depSender = models.TextField(verbose_name='Отдел отправитель:')
    depRec = models.ForeignKey(Group, default=0, on_delete=models.CASCADE, verbose_name='Отдел получатель:')  # Получатель [Временно]

    status = models.ForeignKey(Statuses, default=8, on_delete=models.CASCADE, verbose_name='Статус заявки')  # Статус заявки

    publish = models.DateField(auto_now=False, auto_now_add=False, default=date.today, verbose_name='Дата публикации:')
    readTime = models.IntegerField(default=0, verbose_name='Дата прочтения:')  # models.TimeField(null=True, blank=True) #assume minutes
    updated = models.DateTimeField(auto_now=True, auto_now_add=False, verbose_name='Обнавлено:')
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)

    class Meta:
        ordering = ['order']
        verbose_name = 'Заявка'
        verbose_name_plural = 'Заявка'

    objects = OrderManager()

    def __unicode__(self):
        return self.title

    def __str__(self):
        return self.title

    def getAbsoluteUrl(self):
        return reverse("order:detail", kwargs={"slug": self.slug})

    def getAPIurl(self):
        return reverse("order-api:detail", kwargs={"slug": self.slug})

    class Meta:
        ordering = ["-timestamp", "-updated"]

    def getMarkdown(self):
        content = self.content
        markdown_text = markdown(content)
        return mark_safe(markdown_text)

    @property
    def comments(self):
        instance = self
        qs = Comment.objects.filter_by_instance(instance)
        return qs

    @property
    def get_content_type(self):
        instance = self
        content_type = ContentType.objects.get_for_model(instance.__class__)
        return content_type


def createSlug(instance, new_slug=None):
    slug = slugify(instance.title)
    if new_slug is not None:
        slug = new_slug
    qs = Order.objects.filter(slug=slug).order_by("-id")
    exists = qs.exists()
    if exists:
        new_slug = "%s-%s" % (slug, qs.first().id)
        return createSlug(instance, new_slug=new_slug)
    return slug


def pre_save_post_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = createSlug(instance)

    if instance.content:
        html_string = instance.getMarkdown()
        read_time_var = getReadTime(html_string)
        instance.read_time = read_time_var


pre_save.connect(pre_save_post_receiver, sender=Order)
