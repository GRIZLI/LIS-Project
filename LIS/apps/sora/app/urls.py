from django.conf.urls import url

from django.urls import path
from . import views

app_name = "sora"

urlpatterns = [
    path('', views.viewsOrder, name='order'),
    path('create/', views.orderCreate),
    path('received/', views.viewsResOrder, name='received'),
    path('<str:slug>/', views.orderDetail, name='detail'),
    path('<str:slug>/edit/', views.orderUpdate, name='update'),
    path('<str:slug>/delete/', views.orderDelete),
    # url(r'^posts/$', "<appname>.views.<function_name>"),
]
